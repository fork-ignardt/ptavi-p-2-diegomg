#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Compute two math functions: raise to the power and logarithm.

The main code of the module takes operands and operation (raise or log)
from the command line, as arguments when running the program.

The format for calling the module from the command line is:
compute.py <operation> <num> [<num2>]
<num2> (exponent for power, base for log) is optional: it is
assumed to be 2 if not specified.

For example:
compute.py power 2 3
8.0
compute.py log 8 2
3.0
"""

import math
import sys

class Compute():

    def __init__(self, default=2):
        """Esto es el método iniciliazador"""
        self.default = default

    def power(num, self):
        """ Function to compute number to the exp power"""
        return num ** self

    def log(num, self):
        """ Function to compute log base of number"""
        return math.log(num, self)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        # Creo un objeto de la clase Compute y le doy el valor de 2
        num2 = Compute().default

    if sys.argv[1] == "power":
        result = Compute.power(num, num2)
    elif sys.argv[1] == "log":
        result = Compute.log(num, num2)
    else:
        sys.exit('Operand should be power or log')

    print(result)